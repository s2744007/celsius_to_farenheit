package nl.utwente.di.bookQuote;

public class Converter {

    public double getFahrenheit(double celsius){
        return (celsius * 9.0/5.0) + 32.0;
    }
}
